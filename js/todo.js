var task = [];
var taskID = '';
var taskid = '';
function addTask(){
    var taskRow ={ "id":document.getElementById("add_id").value, "taskTitle" :document.getElementById("add_task").value };
    task.push(taskRow);
    document.getElementById('add_id').value='';
    document.getElementById('add_task').value='';
    listTasks();
}

function listTasks(){
  var list = '';
  for(i=0;i<task.length;i++){
    list+="<tr>";
    list+="<td>"+task[i].id+"</td>";
    list+="<td>"+task[i].taskTitle+"</td>";
    list+='<td><button type="button" class="btn btn-primary" onclick="editTask('+i+')" data-target="#myModal1" data-toggle="modal">edit</button></td>';
    list+='<td><button type="button" class="btn btn-danger" onclick="deleteTask('+i+')">delete</button></td>';
    list+="</tr>";  
  }
  document.getElementById("table_rows").innerHTML= list;
}

function deleteTask(i){
  task.splice(i,1);
  listTasks();
}

function editTask(i){
  taskid = i;
}

function editT(){
  task[taskid].taskTitle=document.getElementById('edit_task').value;
  document.getElementById('edit_task').value='';
  listTasks();
}
